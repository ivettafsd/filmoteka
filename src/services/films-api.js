import axios from 'axios';

const BASE_URL = 'https://api.themoviedb.org/3';
const API_KEY = '6b8ef447c2ce3d010bfcc7f710d71588';

axios.defaults.baseURL = BASE_URL;
axios.defaults.params = {
  api_key: API_KEY,
};

const fetchFilmsTrending = async () => {
  return await axios.get('trending/all/day?').then(({ data }) => data);
};

const fetchSearchFilms = async (page = 1, searchQuery) => {
  return await axios
    .get(`search/movie?&page=${page}&query=${searchQuery}`)
    .then(({ data }) => data);
};

const fetchFilmId = async movieId => {
  return await axios.get(`movie/${movieId}`).then(({ data }) => data);
};

const fetchCast = async movieId => {
  return await axios.get(`/movie/${movieId}/credits`).then(({ data }) => data);
};

const fetchReviews = async movieId => {
  return await axios.get(`movie/${movieId}/reviews`).then(({ data }) => data);
};

export {
  fetchFilmsTrending,
  fetchSearchFilms,
  fetchFilmId,
  fetchCast,
  fetchReviews,
};
