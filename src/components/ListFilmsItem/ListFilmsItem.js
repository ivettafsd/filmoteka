import PropTypes from 'prop-types';
import styles from './ListFilmsItem.module.css';
import defaultImg from './defaultFilm.png';

const ListFilmsItem = ({ poster_path, title, release_date }) => {
  const imgUrl = poster_path
    ? `https://image.tmdb.org/t/p/w500/${poster_path}`
    : defaultImg;
  const titleFilm = title.length > 40 ? title.slice(0, 40) + '...' : title;
  return (
    <>
      <img className={styles.img} src={imgUrl} alt={title} />
      <h4 className={styles.name}>{titleFilm}</h4>
      <span className={styles.info}>{release_date}</span>
    </>
  );
};

ListFilmsItem.defaultProps = {
  poster_path: defaultImg,
  title: 'noname',
  release_date: 'XXXX-XX-XX',
};

ListFilmsItem.propTypes = {
  poster_path: PropTypes.string,
  title: PropTypes.string,
  release_date: PropTypes.string,
};
export default ListFilmsItem;
