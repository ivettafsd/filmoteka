import { Suspense, lazy } from 'react';
import { NavLink, Route, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import styles from './AdditionalInfoFilm.module.css';

import Toggler from '../Toggler';

const Cast = lazy(() =>
  import('../Cast/Cast.js' /* webpackChunkName: "cast-view" */),
);
const Reviews = lazy(() =>
  import('../Reviews/Reviews.js' /* webpackChunkName: "reviews-view" */),
);

const AdditionalInfoFilm = ({ match }) => {
  return (
    <>
      <div className={styles.container}>
        <h4>Additional information</h4>
        <ul className={styles.list}>
          <li>
            <Toggler
              render={() => (
                <NavLink to={`${match.url}/cast`} className={styles.item}>
                  Cast
                </NavLink>
              )}
            />
          </li>
          <li>
            <Toggler
              render={() => (
                <NavLink to={`${match.url}/reviews`} className={styles.item}>
                  Reviews
                </NavLink>
              )}
            />
          </li>
        </ul>
      </div>

      <Suspense
        fallback={
          <Loader type="ThreeDots" color="#ff6b08" height={80} width={80} />
        }
      >
        <Route exact path={`${match.path}/cast`} component={Cast} />

        <Route exact path={`${match.path}/reviews`} component={Reviews} />
      </Suspense>
    </>
  );
};

AdditionalInfoFilm.propTypes = {
  match: PropTypes.object.isRequired,
};

export default withRouter(AdditionalInfoFilm);
