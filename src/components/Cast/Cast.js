import { Component } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { fetchCast } from '../../services/films-api';
import CardActor from '../CardActor';
import styles from './Cast.module.css';
import './CastShow.css';

class Cast extends Component {
  state = {
    cast: null,
  };

  async componentDidMount() {
    const { movieId } = this.props.match.params;
    fetchCast(movieId).then(data => this.setState({ cast: data.cast }));
  }

  render() {
    return (
      <>
        {this.state.cast && (
          <CSSTransition
            in={true}
            appear={true}
            timeout={500}
            classNames="CastShow"
          >
            <div className={styles.container}>
              <TransitionGroup component="ul" className={styles.list}>
                {this.state.cast.map(item => (
                  <CSSTransition
                    key={item.id}
                    in={true}
                    appear={true}
                    timeout={500}
                    classNames="CastItemShow"
                  >
                    <li className={styles.item}>
                      <CardActor
                        imgUrl={item.profile_path}
                        name={item.name}
                        character={item.character}
                      />
                    </li>
                  </CSSTransition>
                ))}
              </TransitionGroup>
            </div>
          </CSSTransition>
        )}
      </>
    );
  }
}

export default Cast;
