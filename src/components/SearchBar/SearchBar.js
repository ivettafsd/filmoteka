import { Component } from 'react';
import styles from './SearchBar.module.css';

class SearchBar extends Component {
  state = {
    query: '',
  };

  handleChange = event => {
    this.setState({ query: event.currentTarget.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.props.onSubmit(this.state.query);
    this.setState({ query: '' });
  };
  render() {
    return (
      <form className={styles.form} onSubmit={this.handleSubmit}>
        <input
          className={styles.input}
          type="text"
          name="search"
          value={this.state.query}
          placeholder="Search films and serials"
          onChange={this.handleChange}
        />

        <button className={styles.btn} type="submit">
          <span>Search</span>
        </button>
      </form>
    );
  }
}
export default SearchBar;
