import PropTypes from 'prop-types';
import styles from './CardActor.module.css';
import defaultImg from './defaultActor.png';

const CardActor = ({ imgUrl, name, character }) => {
  const url = imgUrl ? `https://image.tmdb.org/t/p/w500/${imgUrl}` : defaultImg;

  return (
    <>
      <img className={styles.img} src={url} alt={name} />
      <h3 className={styles.title}>{name}</h3>
      <p className={styles.text}>Character: {character}</p>
    </>
  );
};

CardActor.defaultProps = {
  imgUrl: defaultImg,
  name: 'Incognita',
  character: '',
};

CardActor.propTypes = {
  imgUrl: PropTypes.string,
  name: PropTypes.string,
  character: PropTypes.string,
};
export default CardActor;
