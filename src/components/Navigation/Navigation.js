import { NavLink } from 'react-router-dom';
import routes from '../../routes';
import styles from './Navigation.module.css';

const Navigation = props => (
  <>
    <NavLink
      className={styles.btn}
      activeClassName={styles.btnActive}
      exact
      to={routes.home}
    >
      Home
    </NavLink>

    <NavLink
      className={styles.btn}
      activeClassName={styles.btnActive}
      to={routes.movies}
    >
      Movies
    </NavLink>
  </>
);
export default Navigation;
