import { Component } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { fetchReviews } from '../../services/films-api';
import CardReview from '../CardReview';
import styles from './Reviews.module.css';
import './ReviewsShow.css';

class Reviews extends Component {
  state = {
    reviews: null,
  };

  async componentDidMount() {
    const { movieId } = this.props.match.params;
    fetchReviews(movieId).then(data =>
      this.setState({ reviews: data.results }),
    );
  }

  render() {
    return (
      <>
        <CSSTransition
          in={true}
          appear={true}
          timeout={500}
          classNames="ReviewsShow"
        >
          <div className={styles.container}>
            {this.state.reviews?.length > 0 ? (
              <TransitionGroup component="ul">
                {this.state.reviews.map(item => (
                  <CSSTransition
                    key={item.id}
                    in={true}
                    appear={true}
                    timeout={500}
                    classNames="ReviewsItemShow"
                  >
                    <li className={styles.review}>
                      <CardReview author={item.author} text={item.content} />
                    </li>
                  </CSSTransition>
                ))}
              </TransitionGroup>
            ) : (
              <h3>Unfortunately, there are no reviews for this film</h3>
            )}
          </div>
        </CSSTransition>
      </>
    );
  }
}

export default Reviews;
