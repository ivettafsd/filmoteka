import PropTypes from 'prop-types';

const CardReview = ({ author, text }) => (
  <div>
    <h4>{author}</h4>
    <p>{text}</p>
  </div>
);

CardReview.propTypes = {
  author: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};
export default CardReview;
