// import withHigherOrderComponent from '../hoc/withHigherOrderComponent';
import withLog from '../hoc/withLog';

const MyComponent = props => <div>{JSON.stringify(props, null, 2)}</div>;

// export default withHigherOrderComponent(MyComponent);
export default withLog(MyComponent);
