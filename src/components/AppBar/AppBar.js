import Navigation from '../Navigation';
import styles from './AppBar.module.css';

const AppBar = props => (
  <header className={styles.header}>
    <Navigation />
  </header>
);

export default AppBar;
