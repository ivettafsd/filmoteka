import { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AdditionalInfoFilm from '../AdditionalInfoFilm';
import defaultImg from './defaultFilm.png';
import styles from './CardFilm.module.css';
import routes from '../../routes';

class CardFilm extends Component {
  state = {
    location: this.props.location,
  };

  handleClick = event => {
    this.props.history.push(this.state.location?.state?.from || routes.home);
  };

  render() {
    const imgUrl = this.props.backdrop_path
      ? `https://image.tmdb.org/t/p/w500/${this.props.backdrop_path}`
      : defaultImg;
    const releaseDate = this.props.release_date.slice(0, 4);
    const userScore = this.props.vote_average * 10;
    const genresFilm = this.props.genres.reduce(
      (acc, genre) => acc + genre.name + ' ',
      '',
    );
    const userScoreClass =
      userScore > 50 ? styles.raitingActive : styles.raitingDisabled;

    return (
      <>
        <div className={styles.container}>
          <Link
            className={styles.btn}
            to={{
              pathname:
                this.state.location?.state?.from.pathname || routes.home,
              state: {
                query: this.state.location?.state?.query,
              },
            }}
          >
            Go back
          </Link>

          <div className={styles.card}>
            <img className={styles.img} src={imgUrl} alt={this.props.title} />

            <div className={styles.content}>
              <h3 className={styles.title}>
                {this.props.title}
                <span>({releaseDate})</span>
              </h3>
              <p>
                User Score: <span className={userScoreClass}>{userScore}%</span>
              </p>
              <h4 className={styles.info}>Overview</h4>
              <p className={styles.info}>{this.props.overview}</p>
              <h4 className={styles.info}>Genres</h4>
              <p className={styles.info}>{genresFilm}</p>
            </div>
          </div>
        </div>
        <AdditionalInfoFilm />
      </>
    );
  }
}

CardFilm.defaultProps = {
  backdrop_path: defaultImg,
  release_date: 'XXXX-XX-XX',
  vote_average: 0,
  overview: '',
  genres: [],
};

CardFilm.propTypes = {
  backdrop_path: PropTypes.string,
  title: PropTypes.string.isRequired,
  release_date: PropTypes.string,
  vote_average: PropTypes.number,
  overview: PropTypes.string,
  genres: PropTypes.array,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default withRouter(CardFilm);
