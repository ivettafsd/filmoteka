import { Link, withRouter } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';
import ListFilmsItem from '../ListFilmsItem';
import styles from './ListFilms.module.css';
import './ListFilmsShow.css';

const ListFilms = ({ title, listFilms, location, query }) => {
  return (
    <>
      {title && <h2 className={styles.title}>{title}</h2>}
      <CSSTransition
        in={true}
        appear={true}
        classNames="ListFilms"
        timeout={600}
      >
        <ul className={styles.list}>
          {listFilms.map(film => (
            <li key={film.id} className={styles.card} data-id={film.id}>
              <Link
                to={{
                  pathname: `/movies/${film.id}`,
                  state: {
                    from: location,
                    query: query,
                  },
                }}
              >
                <ListFilmsItem
                  id={film.id}
                  poster_path={film.poster_path}
                  title={film.title}
                  release_date={film.release_date}
                />
              </Link>
            </li>
          ))}
        </ul>
      </CSSTransition>
    </>
  );
};

ListFilms.defaultProps = {
  title: '',
};

ListFilms.propTypes = {
  title: PropTypes.string,
  listFilms: PropTypes.array.isRequired,
  location: PropTypes.object.isRequired,
};

export default withRouter(ListFilms);
