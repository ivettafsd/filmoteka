import { Suspense, lazy } from 'react';
import { Switch, Route } from 'react-router-dom';
import AppBar from '../AppBar';
import routes from '../../routes';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

const HomeView = lazy(() =>
  import('../../views/HomeView.js' /* webpackChunkName: "home-view" */),
);
const MoviesView = lazy(() =>
  import('../../views/MoviesView.js' /* webpackChunkName: "movies-view" */),
);
const MovieDetailsView = lazy(() =>
  import(
    '../../views/MovieDetailsView.js' /* webpackChunkName: "movieDetails-view" */
  ),
);
const NotFoundView = lazy(() =>
  import('../../views/NotFoundView.js' /* webpackChunkName: "notFound-view" */),
);

const App = () => (
  <>
    <AppBar />
    <Suspense
      fallback={
        <Loader type="ThreeDots" color="#ff6b08" height={80} width={80} />
      }
    >
      <Switch>
        <Route exact path={routes.home} component={HomeView} />
        <Route path={routes.movieDetails} component={MovieDetailsView} />
        <Route path={routes.movies} component={MoviesView} />
        <Route component={NotFoundView} />
      </Switch>
    </Suspense>
  </>
);

export default App;
