import { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';
import { fetchSearchFilms } from '../services/films-api';
import SearchBar from '../components/SearchBar';
import ListFilms from '../components/ListFilms';
import '../components/SearchBar/SearchBarShow.css';

class MoviesView extends Component {
  state = {
    films: [],
    searchQuery: null,
  };

  componentDidMount = () => {
    if (this.props.location.state?.query) {
      this.setState({
        searchQuery: this.props.location.state.query,
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.searchQuery !== this.state.searchQuery) {
      this.fetchFilms();
    }
  }

  onChangeQuery = query => {
    this.setState({
      films: [],
      searchQuery: query,
      page: 1,
    });
  };

  async fetchFilms() {
    fetchSearchFilms(this.state.page, this.state.searchQuery).then(data => {
      this.setState({ films: data.results });
    });
  }

  render() {
    return (
      <>
        <CSSTransition
          in={true}
          appear={true}
          timeout={250}
          classNames="SearchBarShow"
        >
          <SearchBar onSubmit={this.onChangeQuery} />
        </CSSTransition>
        {this.state.films.length > 0 ? (
          <ListFilms
            listFilms={this.state.films}
            query={this.state.searchQuery}
          />
        ) : (
          <h2>Please enter a specific request for further movie search!</h2>
        )}
      </>
    );
  }
}
export default withRouter(MoviesView);
