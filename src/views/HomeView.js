import { Component } from 'react';
import { fetchFilmsTrending } from '../services/films-api';
import ListFilms from '../components/ListFilms';

class HomeView extends Component {
  state = {
    films: [],
  };

  async componentDidMount() {
    fetchFilmsTrending().then(data => this.setState({ films: data.results }));
  }

  render() {
    return (
      <div>
        <ListFilms title={'Trending today'} listFilms={this.state.films} />
      </div>
    );
  }
}

export default HomeView;
