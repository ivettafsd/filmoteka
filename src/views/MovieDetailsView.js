import { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import { fetchFilmId } from '../services/films-api';
import CardFilm from '../components/CardFilm';
import '../components/CardFilm/CardFilmShow.css';

class MovieDetailsView extends Component {
  state = {
    id: null,
    backdrop_path: null,
    title: null,
    release_date: null,
    vote_average: null,
    overview: null,
    genres: null,
  };

  async componentDidMount() {
    const movieId = this.props.match.params.movieId;
    fetchFilmId(movieId).then(data => this.setState({ ...data }));
  }

  render() {
    const {
      backdrop_path,
      title,
      release_date,
      vote_average,
      overview,
      genres,
    } = this.state;
    return (
      <>
        {this.state.id && (
          <CSSTransition
            in={true}
            appear={true}
            timeout={500}
            classNames="CardFilmShow"
          >
            <CardFilm
              backdrop_path={backdrop_path}
              title={title}
              release_date={release_date}
              vote_average={vote_average}
              overview={overview}
              genres={genres}
            />
          </CSSTransition>
        )}
      </>
    );
  }
}

export default MovieDetailsView;
